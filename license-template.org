#+REVEAL_HTML: <div class="slide-footer"><br></div></section><section id="slide-license" data-state="no-toc-progress"><h3 class="no-toc-progress">License Information</h3>
@@latex:\section*{License Information}@@

This presentation is part of an
[[https://en.wikipedia.org/wiki/Open_educational_resources][Open Educational Resource (OER)]]
course on Operating Systems.
[[https://gitlab.com/oer/OS][Source code and source files are available on GitLab]]
under [[https://en.wikipedia.org/wiki/Free_license][free licenses]].

Except where otherwise noted, this work,
“@@html:<span property="dc:title">@@{{{title}}}@@html:</span>@@”,
is © {{{copyrightyears}}} by
@@html:<span property="dc:creator cc:attributionName">@@{{{author}}}@@html:</span>@@,
published under the Creative Commons license @@latex: \href{https://creativecommons.org/licenses/by-sa/4.0/}{CC BY-SA 4.0.}@@
@@html:<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0.</a>@@

#+BEGIN_QUOTE
[[https://creativecommons.org/licenses/by-sa/4.0/#deed-understanding][No warranties are given.  The license may not give you all of the permissions necessary for your intended use.]]
#+END_QUOTE

In particular, trademark rights are /not/ licensed under this license.
Thus, rights concerning third party logos (e.g., on the title slide)
and other (trade-) marks (e.g., “Creative Commons” itself) remain with
their respective holders.
